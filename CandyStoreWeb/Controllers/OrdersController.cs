﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CandyStoreWeb.Controllers
{
    public class OrdersController : Controller
    {
        DBOperations db = new DBOperations();

        // GET: Orders
        public ActionResult Index()
        {
            var orders = db.GetAllOrders();
            return View(orders);
        }

        public ActionResult Create()
        {
			var stockPastries = db.GetAllPastries().Where(pastr => pastr.Stock_ID != null).ToList();

			//stockPastries.Add(new DAL.Pastry()
			//{
			//	PastryType = new DAL.PastryType()
			//	{
			//		Name = "Торт",
			//		ID = 1,
			//	},
			//	ID = 1,
			//	Price = 10,
			//	Quantity = 4,
			//	ShelfLife = 30

			//});
			//stockPastries.Add(new DAL.Pastry()
			//{
			//	PastryType = new DAL.PastryType()
			//	{
			//		Name = "Конфета",
			//		ID = 1,
			//	},
			//	ID = 2,
			//	Price = 120,
			//	Quantity = 40,
			//	ShelfLife = 30

			//});

			return View(stockPastries);
        }

        [HttpPost]
        public ActionResult Create(List<DAL.Pastry> pastries)
        {
            return RedirectToAction("Index");
        }
    }
}