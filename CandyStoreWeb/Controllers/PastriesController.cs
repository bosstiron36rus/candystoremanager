﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DAL;

namespace CandyStoreWeb.Controllers
{
    public class PastriesController : Controller
    {
        // GET: Pastries
        DBOperations db;

        public PastriesController()
        {
            db = new DBOperations();
        }

        public ActionResult Index()
        {
            var pastries = db.GetAllPastries();
            return View(pastries);
        }

        public ActionResult InTheStore()
        {
            var pastries = db.GetAllPastries().Where(pastr => pastr.Stock_ID == null).ToList();
            return View(pastries);
        }

        public ActionResult InTheStock()
        {
            var pastries = db.GetAllPastries().Where(pastr => pastr.Stock_ID != null).ToList();
            return View(pastries);
        }
    }
}