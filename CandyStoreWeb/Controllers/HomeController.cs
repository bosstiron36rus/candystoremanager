﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DAL;

namespace CandyStoreWeb.Controllers
{
    public class HomeController : Controller
    {

        DBOperations db = new DBOperations();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(string login, string password)
        {
            var user = db.GetAllEmployees().Where(emp => emp.Login == login && emp.PasswordHash == Tools.Base64Encode(password)).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("SignIn");
            }
            else
            { 
                Models.User.SetCurrentUser(user.Login, user.Role);
                return RedirectToAction("Index");
            }
        }

        public ActionResult SignOut()
        {
            Models.User.DeleteCurrentUser();
            return RedirectToAction("Index");
        }
    }
}