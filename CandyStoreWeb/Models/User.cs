﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CandyStoreWeb.Models
{
    public sealed class User
    {
        private static User user;

        public string Name { get; set; }

        public string Role { get; set; }

        private User(string name, string role)
        {
            this.Name = name;
            this.Role = role;
        }

        public static User SetCurrentUser(string name, string role)
        {
            if(user == null)
            {
                user = new User(name, role);
            }
            return user;
        }   

        public static User CurrentUser
        {
            get
            {
                return user;
            }
        }

        public static void DeleteCurrentUser()
        {
            if(user != null)
            {
                user = null;
            }
        }
        
    }
}