﻿using BLL;
using DAL;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CandyStoreApp.ViewModels
{
    class StockViewModel : INotifyPropertyChanged
    {
        DBOperations db = new DBOperations();

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private ObservableCollection<Pastry> pastries;
        public ObservableCollection<Pastry> Pastries
        {
            get
            {
                return pastries;
            }
            set
            {
                pastries = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Order> orders;
        public ObservableCollection<Order> Orders
        {
            get
            {
                return orders;
            }
            set
            {
                orders = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Discount> discounts;
        public ObservableCollection<Discount> Discounts
        {
            get
            {
                return discounts;
            }
            set
            {
                discounts = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<Employee> employees;
        public ObservableCollection<Employee> Employees
        {
            get
            {
                return employees;
            }
            set
            {
                employees = value;
                OnPropertyChanged();
            }
        }

        public StockViewModel()
        {
            Pastries = db.GetAllPastries();
            Orders = db.GetAllOrders();
            Discounts = db.GetAllDiscounts();
            Employees = db.GetAllEmployees();
        }

        private DelegateCommand save;
        public DelegateCommand Save
        {
            get
            {
                return save ??
                    (save = new DelegateCommand(obj =>
                    {
                        db.Save();
                    }));
            }
        }

    }
}
