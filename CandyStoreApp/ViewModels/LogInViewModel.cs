﻿using BLL;
using CandyStoreApp.Models;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace CandyStoreApp.ViewModels
{
    class LogInViewModel : INotifyPropertyChanged
    {

        DBOperations db = new DBOperations();

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                OnPropertyChanged();
            }
        }

        private string password;
        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = Tools.Base64Encode(value);
                OnPropertyChanged();
            }
        }



        private DelegateCommand logIn;
        public DelegateCommand LogIn
        {
            get
            {
                return logIn ??
                  (logIn = new DelegateCommand(obj =>
                 {
                     PasswordBox passwordBox = obj as PasswordBox;
                     this.Password = passwordBox.Password;

                     var employee = db.GetEmployee(this.UserName, this.Password);

                     if (employee != null)
                     {
                         MainWindow mainWindow = new MainWindow();
                         CurrentUser.FullName = employee.FullName;
                         CurrentUser.Role = employee.Role;
                         mainWindow.DataContext = new MainViewModel();
                         mainWindow.ShowDialog();
                     }
                 }));
            }
        }
    }
}
