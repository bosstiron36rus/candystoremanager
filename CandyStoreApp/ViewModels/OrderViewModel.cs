﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DAL;
using BLL;
using BLL.Services;
using CandyStoreApp.Models;

namespace CandyStoreApp.ViewModels
{
    class OrderViewModel : INotifyPropertyChanged
    {
        DBOperations db = new DBOperations();
        OrdersService ordersService;

        public OrderViewModel(List<Pastry> pastries)
        {
            ordersService = new OrdersService(db);
            PastriesToOrder = new List<PastryToOrder>();
            var pastriesFromStock = db.GetAllPastries().Where(p => p.Stock_ID != null && pastries.Any(past => past.PastryType_ID == p.PastryType_ID)).ToList();
            foreach (var pastry in pastriesFromStock)
            {
                PastriesToOrder.Add(new PastryToOrder { Pastry = pastry, Quantity = 0 });
            }
            SelectedPastry = PastriesToOrder.First();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }


        private List<PastryToOrder> pastriesToOrder;
        public List<PastryToOrder> PastriesToOrder
        {
            get
            {
                return pastriesToOrder;
            }
            set
            {
                pastriesToOrder = value;
                OnPropertyChanged();
            }
        }

        public PastryToOrder selectedPastry;
        public PastryToOrder SelectedPastry
        {
            get { return selectedPastry; }
            set
            {
                selectedPastry = value;
                OnPropertyChanged();
            }
        }

        private DelegateCommand makeOrder;
        public DelegateCommand MakeOrder
        {
            get
            {
                return makeOrder ??
                  (makeOrder = new DelegateCommand(obj =>
                  {
                      Order order = new Order();
                      order.Employee = db.GetAllEmployees().Where(e => e.FullName == CurrentUser.FullName).FirstOrDefault();
                      foreach (var pastry in PastriesToOrder)
                      {
                          var orderLine = new OrderLine()
                          {
                              Pastry_ID = pastry.Pastry.ID,
                              Pastry = db.GetPastry(pastry.Pastry.ID),
                              Price = pastry.Pastry.Price * pastry.Quantity,
                              Quantity = pastry.Quantity,
                              Order_ID = order.ID,
                          };
                          order.OrderLines.Add(orderLine);
                          pastry.Pastry.Quantity -= pastry.Quantity;
                      }
                      ordersService.MakeOrder(order);
                      var window = obj as Window;
                      window.DialogResult = true;
                      window.Close();
                  }));
            }
        }
    }
}
