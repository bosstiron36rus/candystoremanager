﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CandyStoreApp.ViewModels
{
    class DiscountViewModel : INotifyPropertyChanged
    {
        DBOperations db;

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public DiscountViewModel(Pastry pastry, DBOperations db)
        {
            this.db = db;
            DiscountPastry = pastry;
            NewDiscount = new Discount();
            NewDiscount.StartDate = DateTime.Now;
            NewDiscount.EndDate = DateTime.Now.AddDays(3);
            if (DiscountPastry.Discount != null)
            {
                NewDiscount.DiscountPercent = DiscountPastry.Discount.DiscountPercent;
                NewDiscount.ID = DiscountPastry.Discount.ID;
            }
        }

        private Pastry discountPastry;
        public Pastry DiscountPastry
        {
            get { return discountPastry; }
            set
            {
                discountPastry = value;
                OnPropertyChanged();
            }
        }

        private Discount newDiscount;
        public Discount NewDiscount
        {
            get { return newDiscount; }
            set
            {
                newDiscount = value;
                OnPropertyChanged();
            }
        }

        private DelegateCommand addDiscount;
        public DelegateCommand AddDiscount
        {
            get
            {
                return addDiscount ??
                  (addDiscount = new DelegateCommand(obj =>
                  {
                      DiscountPastry.Discount = NewDiscount;
                      DiscountPastry.Discount_ID = DiscountPastry.ID;
                      db.UpdatePastry(DiscountPastry);
                      var window = obj as Window;
                      window.DialogResult = true;
                      window.Close();
                  }));
            }
        }
    }
}
