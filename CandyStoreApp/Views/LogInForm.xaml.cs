﻿using System.Windows;

namespace CandyStoreApp
{
    /// <summary>
    /// Логика взаимодействия для LogInForm.xaml
    /// </summary>
    public partial class LogInForm : Window
    {
        public LogInForm()
        {
            InitializeComponent();

            DataContext = new ViewModels.LogInViewModel();
        }
    }
}
