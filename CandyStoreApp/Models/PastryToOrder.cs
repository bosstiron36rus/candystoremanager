﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandyStoreApp.Models
{
    internal class PastryToOrder
    {
        public Pastry Pastry { get; set; }
        public int Quantity { get; set; }
    }
}
