﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandyStoreApp.Models
{
    public static class CurrentUser
    {
        public static string FullName { get; set; }

        public static string Role { get; set; }
    }
}
