namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CandyStoreDB : DbContext
    {
        public CandyStoreDB()
            : base("name=DBConnection")
        {
        }

        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderLine> OrderLines { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<Pastry> Pastries { get; set; }
        public virtual DbSet<PastryType> PastryTypes { get; set; }
        public virtual DbSet<Stock> Stocks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .Property(e => e.FullName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Login)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.PasswordHash)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Phone)
                .IsFixedLength();

            modelBuilder.Entity<Employee>()
                .Property(e => e.Role)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.Employee)
                .HasForeignKey(e => e.Employee_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderLines)
                .WithRequired(e => e.Order)
                .HasForeignKey(e => e.Order_ID);

            modelBuilder.Entity<OrderStatus>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<OrderStatus>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.OrderStatus)
                .HasForeignKey(e => e.Status_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pastry>()
                .HasOptional(e => e.Discount)
                .WithRequired(e => e.Pastry);

            modelBuilder.Entity<Pastry>()
                .HasMany(e => e.OrderLines)
                .WithRequired(e => e.Pastry)
                .HasForeignKey(e => e.Pastry_ID);

            modelBuilder.Entity<PastryType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PastryType>()
                .HasMany(e => e.Pastries)
                .WithRequired(e => e.PastryType)
                .HasForeignKey(e => e.PastryType_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Stock>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Stock>()
                .Property(e => e.Phone)
                .IsFixedLength();

            modelBuilder.Entity<Stock>()
                .HasMany(e => e.Pastries)
                .WithOptional(e => e.Stock)
                .HasForeignKey(e => e.Stock_ID);
        }
    }
}
