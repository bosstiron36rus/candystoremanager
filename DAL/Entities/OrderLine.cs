namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderLine")]
    public partial class OrderLine
    {
        public int ID { get; set; }

        public int Quantity { get; set; }

        public int Pastry_ID { get; set; }

        public double Price { get; set; }

        public int Order_ID { get; set; }

        public virtual Order Order { get; set; }

        public virtual Pastry Pastry { get; set; }
    }
}
