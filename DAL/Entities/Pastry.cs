namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Pastry")]
    public partial class Pastry
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pastry()
        {
            OrderLines = new HashSet<OrderLine>();
        }

        public int ID { get; set; }

        public double ShelfLife { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        public int PastryType_ID { get; set; }

        public DateTime ManufactureDate { get; set; }

        public int? Stock_ID { get; set; }

        public int? Discount_ID { get; set; }

        public virtual Discount Discount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderLine> OrderLines { get; set; }

        public virtual PastryType PastryType { get; set; }

        public virtual Stock Stock { get; set; }
    }
}
