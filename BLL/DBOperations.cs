﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class DBOperations
    {
        CandyStoreDB db;

        public DBOperations()
        {
            this.db = new CandyStoreDB();
        }

        public ObservableCollection<Pastry> GetAllPastries()
        {
            db.Pastries.Load();
            return db.Pastries.Local;
        }

        public Pastry GetPastry(int id)
        {
            return db.Pastries.Find(id);
        }

        public int CreateDiscount(Discount discount)
        {
            db.Discounts.Add(discount);
            Save();
            return discount.ID;
        }

        public bool UpdatePastry(Pastry pastry)
        {
            db.Entry(pastry).State = EntityState.Modified;
            return Save();
        }

        public bool DeletePastry(int id)
        {
            Pastry pastry = db.Pastries.Find(id);
            if(pastry != null)
            {
                db.Pastries.Remove(pastry);
            }
            return Save();
        }

        public bool CreatePastry(Pastry pastry)
        {
            db.Pastries.Add(pastry);
            return Save();
        }

        public ObservableCollection<Order> GetAllOrders()
        {
            db.Orders.Load();
            return db.Orders.Local;
        }

        public Order GetOrder(int id)
        {
            return db.Orders.Find(id);
        }

        public bool UpdateOrder(Order order)
        {
            db.Entry(order).State = EntityState.Modified;
            return Save();
        }

        public bool DeleteOrder(int id)
        {
            Order order = db.Orders.Find(id);
            if (order != null)
            {
                db.Orders.Remove(order);
            }
            return Save();
        }

        public bool DeleteDiscount(int id)
        {
            Discount discount = db.Discounts.Find(id);
            if(discount != null)
            {
                db.Discounts.Remove(discount);
            }
            return Save();
        }

        public bool CreateOrder(Order order)
        {
            db.Orders.Add(order);
            return Save();
        }

        public Employee GetEmployee(string login, string password)
        {
            db.Employees.Load();
            return db.Employees.Local.FirstOrDefault(emp => emp.Login == login && emp.PasswordHash == password);
        }

        public ObservableCollection<Discount> GetAllDiscounts()
        {
            db.Discounts.Load();
            var discountsToDelete = db.Discounts.Local.Where(d => DateTime.Now > d.EndDate).ToList();
            foreach(var discount in discountsToDelete)
            {
                db.Discounts.Remove(discount);
            }
            Save();
            return db.Discounts.Local;
        }

        public bool Save()
        {
            if (db.SaveChanges() > 0) return true;
            return false;
        }
    }
}
