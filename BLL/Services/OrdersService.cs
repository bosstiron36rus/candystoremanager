﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class OrdersService
    {
        DBOperations db;

        public OrdersService(DBOperations db)
        {
            this.db = db; 
        }

        public bool MakeOrder(Order order)
        {
            var currentDate = DateTime.Now;
            foreach(var orderline in order.OrderLines)
            if(orderline.Pastry.Discount != null && currentDate >= orderline.Pastry.Discount.StartDate && currentDate <= orderline.Pastry.Discount.EndDate)
            {
                    orderline.Price = orderline.Price - orderline.Price * orderline.Pastry.Discount.DiscountPercent;
            }
            order.Status_ID = 1;
            order.Date = currentDate;

            return db.CreateOrder(order);
        }

    }
}
